#!/bin/bash
#
# Use this shell script to compile (if necessary) your code and then execute it. Below is an example of what might be found in this file if your program was written in Python
#
python3 ./src/donations.py --i ./input/itcont.txt --p ./input/percentile.txt --o ./output/repeat_donors.txt
