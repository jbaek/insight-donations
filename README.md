# Dependencies
* Tested on Python 3.6.4
* Only standard packages are needed for general use
* memory\_profiler and tracemalloc were used for analyzing memory usage
* cProfile, pstats, and timeit were used for profiling
* pytest was used for unit testing

# Execution

`./run.sh`

# Unit testing with pytest 

`./pytests/pytest`

# Design Considerations

## Data Structures

Two dictionaries store data as it is read from the file and allow for fast lookups. If the proportion of repeat donors dramatically increases, consider different data structure for space considerations. 

1. donors

   * Used to check if the donor had donated in a previous year
   * dict and set lookups are constant time on average
   * key: a namedtuple of name and zipcode
   * value: a set of years contributions were made

2. contrib\_flows

   * Stores the contributions made by repeat donors in a zipcode to a specific recipient in a calendar year
   * dict lookups are constant time on average
   * contribution amounts are stored in sorted order to speed up percentile calculations.
   * bisect package used to efficiently insert new contribution amounts to maintain order
   * binary search is O(log(n)) + O(n) for the insert, preferred over sorting (O(n log(n))) after every insert
   * key: namedtuple of cmte\_id, zipcode, year
   * value: ordered list of contribution amounts

# Future Work

* try storing contribution amounts in numpy array
* fail gracefully, save state, and restart from failure point
* put profiling data in a more readable format
* consider converting data structures to classes
