
""" test_datatransform.py 
unit test data transform functions
using pytest
contrib: a template dict of good data
"""

import sys
sys.path.append('/Users/jasonbaek/projects/insight-donations')
import src.donations as donations

valid_contrib = {"CMTE_ID": "C00177436",
           "NAME": "MCGARRY, JOHN",
           "ZIP_CODE": "040424132",
           "TRANSACTION_DT": "01312017",
           "TRANSACTION_AMT": "384",
           "OTHER_ID": "",}

error_contrib = {"CMTE_ID": "",
           "NAME": "MCGARRY, JOHN",
           "ZIP_CODE": "0A0424132",
           "TRANSACTION_DT": "01512017",
           "TRANSACTION_AMT": "384",
           "OTHER_ID": "XYZ",}

def test_transform_data_year():
    contrib = donations.transform_data(valid_contrib)
    assert contrib.get('CONTRIB_YEAR') == '2017' 

def test_transform_data_zipcode():
    contrib = donations.transform_data(valid_contrib)
    assert contrib.get('ZIP_CODE') == '04042' 

def test_parse_file():
   line = "C00384516|N|M2|P|201702039042412112|15|IND|ABBOTT, JOSEPH|WOONSOCKET|RI|028956146|CVS HEALTH|EVP, HEAD OF RETAIL OPERATIONS|01122018|333||2017020211435-910|1147467|||4020820171370030287"
   assert len(donations.parse_file(line)) == 6
