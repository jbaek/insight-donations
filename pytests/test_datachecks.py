""" test_datachecks.py 
unit test data checking functions
using pytest
contrib: a template dict of good data
"""

import sys
sys.path.append('/Users/jasonbaek/projects/insight-donations')
import src.donations as donations

valid_contrib = {"CMTE_ID": "C00177436",
           "NAME": "MCGARRY, JOHN",
           "ZIP_CODE": "040424132",
           "TRANSACTION_DT": "01312017",
           "TRANSACTION_AMT": "384",
           "OTHER_ID": "",}

error_contrib = {"CMTE_ID": "",
           "NAME": "MCGARRY, JOHN",
           "ZIP_CODE": "0A0424132",
           "TRANSACTION_DT": "01512017",
           "TRANSACTION_AMT": "384",
           "OTHER_ID": "XYZ",}

def test_valid_contribution():
    assert donations.is_valid_contribution(valid_contrib)

def test_valid_date():
    assert donations.is_valid_date(valid_contrib["TRANSACTION_DT"])

def test_date_error():
    assert not donations.is_valid_date(error_contrib['TRANSACTION_DT'])

def test_valid_zipcode():
    assert donations.is_valid_zipcode(valid_contrib["ZIP_CODE"])

def test_zipcode_error_nonnumeric():
    assert not donations.is_valid_zipcode(error_contrib['ZIP_CODE'])

def test_zipcode_error_short():
    assert not donations.is_valid_zipcode('3454')

def test_fields_blank():
    assert donations.are_fields_blank(valid_contrib)

def test_fields_blank_error():
    assert not donations.are_fields_blank(error_contrib)

def test_fields_populated():
    assert donations.are_fields_populated(valid_contrib)

def test_fields_not_populated_error():
    assert not donations.are_fields_populated(error_contrib)


