""" test_datastructures.py 
unit test functions to manage data structures
using pytest
contrib: a template dict of good data
"""

from collections import namedtuple
import sys
sys.path.append('/Users/jasonbaek/projects/insight-donations')
import src.donations as donations


def test_is_repeat_donor():
    Donor = namedtuple('Donor', ['name', 'zipcode'])
    donor = Donor('Miles Davis', '07675')
    donations.donors = {Donor('Miles Davis', '07675'): set([2017])}
    assert donations.is_repeat_donor(donor, 2018)

def test_add_contribution():
    ContributionFlow = namedtuple('ContributionFlow', ['cmte_id', 'zip_code', 'year'])
    contrib_flow = ContributionFlow('A123', '07675', '2017')
    donations.contrib_flows = {ContributionFlow('A123', '07675', '2017'): [35, 99]}
    contribs = donations.add_contribution(contrib_flow, 55)
    assert len(contribs) == 3 
    assert contribs[1] == 55

def test_calculate_measures():
    contribs = [10, 20, 100] 
    percentile = 20
    measures = donations.calculate_measures(contribs, percentile)
    assert measures == [10, 130, 3]

def test_format_output():
    ContributionFlow = namedtuple('ContributionFlow', ['cmte_id', 'zip_code', 'year'])
    contrib_flow = ContributionFlow('A123', '07675', '2017')
    measures = [10, 130, 3] 
    output = donations.format_output(contrib_flow, measures)
    assert output == 'A123|07675|2017|10|130|3'
