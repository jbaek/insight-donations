#!/usr/local/bin/python3
""" donations.py
"""
import logging
import json
from datetime import datetime
from collections import namedtuple
import math
from decimal import Decimal
import bisect
import sys
import argparse
import time
import cProfile
import pstats
import tracemalloc
import csv
# from memory_profiler import profile

# percentile nearest rank: value in list of size N such that no more than P percent of data is strictly less than the value
# calculate the ordinal rank (ceiling of P/100 * N), then take the value in list that corresponds to the rank, round value to the nearest whole dollar

""" donors: dict to store all donors and the years they contributed
key: Donor namedtuple (name, zipcode)
value: list of years contributed
"""
donors = {}

""" contrib_flows: dict to store the contributions from a zipcode to a recipient
in a given year
key: ContributionFlow namedtuple (cmte_id, zipcode, year)
value: list of contribution amounts stored in sorted order to speed up percentile
       calculations
"""
contrib_flows = {}

""" summarystats: dict to store summary statistics
key: summary name
value: statistic value
"""
summarystats = {'valid_contrib': 0, 'error_contrib': 0, 'num_repeat_donors': 0,
                'num_repeat_donors': 0, 'max_num_repeat_contrib': 0}


def is_valid_date(date_string):
    """ Checks if the date has the MMDDYYYY format
    :param date_string: input text string
    :returns: boolean
    """
    try:
        formatted_date = datetime.strptime(date_string, '%m%d%Y').date()
        return True
    except ValueError:
        logging.debug(f"TRANSACTION_DT is not valid: {date_string}")
        return False


def is_valid_zipcode(zipcode):
    """ Checks if the zip code has at least 5 characters and all are digits
    :param zipcode: input text string
    :returns: boolean
    """
    if len(zipcode) < 5 or not zipcode[:5].isdigit():
        logging.debug(f"ZIP_CODE is too short or is nonnumeric: {zipcode}")
        return False 
    return True 


def are_fields_blank(contrib):
    """ Certain fields in the contribution must be blank: OTHER_ID
    :param contrib: dict of a single contribution
    :returns: boolean
    """
    if contrib.get('OTHER_ID'): 
        logging.debug(f"OTHER_ID is populated: {contrib.get('OTHER_ID')}")
        return False
    return True 


def is_empty(key, value):
    """ Checks if value is empty
    :param key: name of field
    :param value: value of field
    :returns boolean
    """
    if not value: 
        logging.debug(f"{key} is empty")
        return True 
    return False 


def are_fields_populated(contrib):
    """ Certain fields in the contribution must be populated. Will return False 
    if any are empty
    :param contrib: dict of contribution
    :returns: boolean
    """
    nonempty_fields = ['CMTE_ID', 'NAME', 'TRANSACTION_AMT']
    if any([is_empty(k, v) for k, v in {k: contrib[k] for k in nonempty_fields}.items()]):
       return False 
    return True 


def is_valid_contribution(contrib):
    """ Runs all data quality checks
    :param contrib: dict of contribution
    :returns: boolean
    """
    return all([are_fields_blank(contrib), are_fields_populated(contrib),
                is_valid_zipcode(contrib.get('ZIP_CODE')),
                is_valid_date(contrib['TRANSACTION_DT']),])


def transform_data(contrib):
    """ extracts year from transaction date and extracts the 5 digit zip code
    :param contrib: dict of contribution
    :returns: contrib with the year added
    """
    contrib['CONTRIB_YEAR'] = contrib.get('TRANSACTION_DT')[-4:]
    contrib['ZIP_CODE'] = contrib.get('ZIP_CODE')[:5] 
    return contrib


def parse_file(line):
    """ Takes single line read from file, splits by pipe delimeter, removes
    unused fields, and outputs a dict of field names as the key and data 
    from file as the value
    :param line: single line from file
    :returns: dict of contribution
    """

    fieldnames = ['CMTE_ID', 'NAME', 'ZIP_CODE', 'TRANSACTION_DT',
                  'TRANSACTION_AMT', 'OTHER_ID']
    fieldslist = [0, 7, 10, 13, 14, 15]
    # line_list = line.split('|')
    line_list = line
    contrib_list = [line_list[i] for i in fieldslist]
    contrib = {name: value for name, value in zip(fieldnames,
                                                  contrib_list)} 
    logging.debug(json.dumps(contrib, indent=4))
    return contrib


def get_percentile(percentilefile = 'input/percentile.txt'):
    """ get desired percentile for analysis read from file
    :param percentilefile: can be read from command line args
    :returns: int
    """
    with open(percentilefile) as percentilef:
        percentile = percentilef.read().rstrip('\n')
        percentile = int(percentile)
        logging.info(f"Percentile: {percentile}")
        return percentile


def is_repeat_donor(donor, year):
    """ check if the donor (name and zipcode) have donated in a previous year
    donations in file may not be in chronological order, so years must be 
    checked
    :param donor: Donor namedtuple(name, zipcode); key for lookup in donors
    :param year: year of contribution being checked
    :returns: boolean
    """
    logging.debug(donors.get(donor))
    donated_prior_year = False 
    if donors.get(donor):
        donation_years = [year > prev_year for prev_year in donors.get(donor)]
        donated_prior_year = any(donation_years)
    if donated_prior_year:
        logging.debug(f"{donor} has previously donated in {donors[donor]}")
    return donated_prior_year 


def add_contribution(contrib_flow, transaction_amount):
    """ Add contribution amounts to list for the (zip code, recipient,
    calendar year) in sorted order to speed up percentile calc. 
    Update the global store of contribution flows
    :param contrib_flow: key of contrib_flows;
           dict of contributions from a zip code to a recipient for a given calendar year
    :param transaction_amount: as a string
    :returns: nothing because contrib_flows stored globally 
    """
    contrib_flows.setdefault(contrib_flow, list())
    contribs = contrib_flows.get(contrib_flow)
    bisect.insort(contribs, Decimal(transaction_amount))
    contrib_flows[contrib_flow] = contribs
    return contribs
    

def calculate_percentile(contribs, percentile):
    """ Calculate the percentile using the nearest rank method.
    Assumes that the input list is sorted
    :param contribs: sorted list of contribution amounts
    :param percentile: requested percentile
    :returns: the contribution of the requested percentile
    """
    return round(contribs[math.ceil(percentile * len(contribs) / 100) - 1])


def calculate_measures(contribs, percentile):
    """ Calculate the measures of donations made in a year from a zipcode
    to a recipient; measures will be written to output file
    num_contribs: number of contributions 
    total_contribs: total amount of contributions
    percentile_contrib: see calculate_percentile()
    :param contribs: list of donation amounts
    :param percentile: number of percentile
    :returns list of calculated measures
    """
    num_contribs = len(contribs)
    total_contribs = sum(contribs)
    logging.debug(f"Total # contributions from repeat donors: {len(contribs)}")
    logging.debug(f"Total $ amount of contributions from repeat donors: {sum(contribs)}")
    percentile_contrib = calculate_percentile(contribs, percentile)
    logging.debug(f"{percentile} percentile contribution is {percentile_contrib}")
    return [percentile_contrib, total_contribs, num_contribs]


def format_output(contrib_flow, measures):
    """ Format the output to be written to file
    :param contrib_flow: ContributionFlow namedtuple
    :param measures: see calculate_measures()
    :returns pipe delimited string
    """
    measures = [str(m) for m in measures]
    measures = '|'.join(measures)
    output = '|'.join(contrib_flow)
    return f"{output}|{measures}"


def parse_command_args():
    """ Parse command line arguments
    -i: input file
    -o: output file
    -p: percentile file
    :returns: an ArgumentParser
    """
    argparser = argparse.ArgumentParser()
    argparser.add_argument('--p', help='percentile file')
    argparser.add_argument('--i', help='input file')
    argparser.add_argument('--o', help='output file')
    return argparser.parse_args()

def add_final_stats(starttime):
    summarystats['num_repeat_donors'] = len([key for key, value in donors.items() if len(value) > 1])
    summarystats['max_num_repeat_contrib'] = max({len(value) for key, value in contrib_flows.items()})
    summarystats['total_runtime_sec'] = time.time() - starttime
    logging.info(json.dumps(summarystats, indent=4))

# @profile
def main():
    starttime = time.time()
    logname = 'output/donations.log'
    logging.basicConfig(filename=logname,
                        filemode='a',
                        format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                        datefmt='%H:%M:%S',
                        level=logging.INFO)

    args = parse_command_args()
    inputfile = args.i
    outputfile = args.o
    percentilefile = args.p

    percentile = get_percentile(percentilefile)

    with open(inputfile) as inputf, open(outputfile, 'w') as outputf:
        Donor = namedtuple('Donor', ['name', 'zipcode'])
        ContributionFlow = namedtuple('ContributionFlow', ['cmte_id', 'zip_code', 'year'])
        # Read file into memory one line at a time
        csvreader = csv.reader(inputf, delimiter='|')
        for line in csvreader:
            contrib = parse_file(line)
            if is_valid_contribution(contrib):
                summarystats['valid_contrib'] = summarystats.get('valid_contrib') + 1
                contrib = transform_data(contrib)
                name = contrib.get('NAME')
                zipcode = contrib.get('ZIP_CODE')
                year = contrib.get('CONTRIB_YEAR')
                donor = Donor(name, zipcode)
                contrib_flow = ContributionFlow(contrib.get('CMTE_ID'), zipcode, year)
                if is_repeat_donor(donor, year):
                    transaction_amount = contrib.get('TRANSACTION_AMT')
                    contribs = add_contribution(contrib_flow, transaction_amount)
                    logging.debug(contribs)
                    measures = calculate_measures(contribs, percentile)
                    output = format_output(contrib_flow, measures)
                    outputf.write(output + '\n')
                    logging.debug(contrib_flows)
                donors.setdefault(donor, set()).add(year) 
                logging.debug(f"GOOD INPUT: {contrib}")
            else:
                summarystats['error_contrib'] = summarystats.get('error_contrib') + 1
    add_final_stats(starttime)

if __name__ == '__main__':
    # tracemalloc.start()
    main()
    # snapshot = tracemalloc.take_snapshot()
    # top_stats = snapshot.statistics('lineno')
    # print("[ Top 10 ]")
    # for stat in top_stats[:10]:
    #     print(stat)

    # cProfile.run('main()', 'donation_profile')
    # p = pstats.Stats('donation_profile')
    # p.strip_dirs().sort_stats('cumulative').dump_stats('donation_stats')
